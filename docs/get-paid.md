# Get paid
While we all hope to find value in our work and effort beyond money, most of us want a paycheck or reimbursement for costs accrued related to our work. This section includes information for how to get paid.

## 1099 independent contractors
#### Payment cadence
Payroll runs the first Monday of the month.
#### Payment options 
Contractors have the option to receive payment in $USD via wire transfer or check, or via cryptocurrency.
#### Reporting hours
Contractors must submit hours worked on a weekly basis via Orange HRM. 

## W2 full-time employees
#### Payment cadence
Payroll runs twice a month on the 1st and 16th.
#### Payment options
We pay W2 employees in $USD via direct deposit to your specified bank account.

## Reimbursement
You may incur costs related to your work that are eligible for reimbursement. 
#### Responsibilities
* **Confirm an expense is eligible for reimbursement.** It is your responsibility to confirm an expense can be reimbursed before you make the purchase. Failure to do so may result in a denial of a reimbursement 
* **Submit an expense report.** Please make a copy of the [Pyrofex expense report form](https://docs.google.com/spreadsheets/d/1uy1VLhEj29yNYaezBoRR9mcxvW83E3fNU3ok8_MFy_k/edit#gid=1736794199) and submit it along with a copy of your receipt(s) via email to pyrofex-fops@pyrofex.net.
#### Payment options
You have the option to receive reimbursement in $USD via check or cryptocurrency. Please indicate your preference in the expense report form.
## Questions?
Contact either Tysha (hiring) or Jacquelyn (finance) if you have questions about getting paid.

