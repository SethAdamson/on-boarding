# Communication channels
We use a variety of channels for communications. This section includes information about these channels, how to access, and suggestions for their use.

## Chat
We use [Telegram Messenger](https://telegram.org) for chat.

### Installing Telegram Messenger
1. Install [Telegram Messenger](https://telegram.org) on your device(s).
2. Give your preferred cell number to Nash.

### Telegram Messenger Groups
We use groups to support all-company and small group chat. Talk with your co-workers about groups you should be included in. You're also welcome to create new groups.

### Philosophy on chat
Chat is a helpful tool and serves us well for just in time and quick conversations with people who are available now or will be in the near future. A concern with chat is that it is not the ideal platform for capturing decisions, specifications, requirements, and bug. This type of content must be documented in the appropriate document, wiki, or GitLab issue. If you are in a chat thread that seems like it should be captured, please take the initiative to move the relevant content to a more appropriate platform.

#### A test for determining if a conversation should take place via chat or another platform

 - Are you experiencing a bug or problem, or have an idea for an improvement or something new?
	 - **Yes** First document your experience/problem/suggestion.
	 - **No** See next question.
 - Is your experience/problem/suggestion something that will be discussed in a meeting?
	 - **Yes** Document your experience/problem/suggestion before the meeting and give meeting participants an opprotunity to read before the meeting.
	 - **No** See next question
 - Is everyone who should see your experience/problem/suggestion online, and can it be resolved in 5 minutes or less?
	 - **Yes** Chat is fine for this. Please document any outcomes (decisions, additional questions, next steps, etc.).
	 - **No** Document your experience/problem/suggestiona and share it with relevant stakeholders. Do not use chat for conversations if not everyone is available and if the issue cannot be resolved in 5 minutes or less.

## Email
As a Pyrofex full-time employee or contractor you have an @pyrofex.net account. You are responsible for attending to messages sent to this account in a timely way.

### Email access
You will receive an @pyrofex.net account when you start working with Pyrofex. The invitation will be sent to your personal email. 

You will need [Google 2-step verification](https://www.google.com/landing/2step/) to set up your account

### Email groups
We use Google groups to create email aliases for groups. You can also create these by using Google groups.
