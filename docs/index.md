# Welcome to the Pyrofex handbook
This handbook is primarily for Pyrofex exmployees and contractors.

If you're looking for the Pyrofex handbook, please go to [URL TBD](URL TBD).

## Using the handbook

This repo contains the content of the Pyrfox handbook. For a better reading experience please go to [URL TBD](URL TBD). 

We welcome your thoughts and suggestions. To learn more about sending feedback or suggesting updates to this handbook, please see the Contributing file.

## Creating a new file

This handbook is a collection of Markdown files. This is a plaintext format that easily converts to HTML. Read more about it [here](http://daringfireball.net/projects/markdown/). Be sure you name your files without spaces with either an `.md` or `.markdown` extension.

The files in this guide are prefixed with a double digit to set order. Make sure your file starts with a number that will place your new content in the desired position (eg: `04-`) and change the prefixes of the other files to reflect this change in order.

## Markdown

We write content in this repo in Markdown. Markdown is handy because you can write your content without HTML tags. Learning basic Markdown is not difficult, and most documents don't require more than a basic understanding of Markdown. There are editors you can use to help develop your content. We recommend [StackEdit](https://stackedit.io/app)
