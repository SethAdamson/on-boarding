# Printing
You have access to the printer in the Pyrofex office.

## Quick start
Use the following information to configure the printer for your device.
* Printer type - Cannon 400if
* Printer IP address - 10.64.8.12
* Printer driver - Cannon IR-ADV 400/500. [Download here](https://www.usa.canon.com/internet/portal/us/home/support/details/copiers-mfps-fax-machines/multifunction-copiers/imagerunner-advance-400if?tab=drivers_downloads).

## More details for Mac users
Printer set up for Mac is not straightforward. Please use this additional guidance.

#### Instructions
1. Download and install the driver for the printer. The driver you need is for  **Cannon IR-ADV 400/500**. [Download here](https://www.usa.canon.com/internet/portal/us/home/support/details/copiers-mfps-fax-machines/multifunction-copiers/imagerunner-advance-400if?tab=drivers_downloads).
2. From **System preferences** select **Printers and scanners**.
3. Select **+** to add a new printer.
4. Select **IP** in the toolbar and then from **Protocol** select **Line printer deamon - LPD**
5. In the **Address** field enter the **IP address** 10.64.8.12.
6. From the **Use** field select **Select software** and then select **Cannon IR-ADV 400/500**.