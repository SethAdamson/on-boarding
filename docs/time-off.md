# Leave and time off
We all need a break sometimes to attend to life issues, celebrate holidays, or get some rest and relaxation. At Pyrofex we extend trust to everyone to make decisions about taking time off that are reasonable, responsible, and reflect an understanding of the needs of current work and people you work with.

## Holidays
Pyrofex does not observe any holidays. If there's a holiday you celebrate and want to take time off for, please notify your teammates and update your calendar and the PTO calendar with your plans.

## Vacation
Pyrofex does not specify an amount of vacation time. If you need vacation time, please discuss with your teammates and Mike or Nash. When you make a plan for vacation, please update your calendar and the PTO calendar with your plans.

## Personal time off
We all have commitments outside of work. If you need to attend to these during your typical work ours, please notify your teammates and update your calendar and the PTO calendar with your plans.

## PTO calendar
The PTO calendar is a calendar include in the G-Suite tools we use at Pyrofex. Commonly referred to as the PTO calendar, the formal name for this calendar in G-Suite is Personal Time Off.

#### Accessing the PTO calendar
From the Google calendar associated with your @pyrofex.net account you can:
 ##### View the PTO calendar 
 From the field **Add a coworker's calendar**, type **Personal Time Off** to view the calendar. Once you've viewed it once, you can view it again by selecting the checkbox next to **Personal Time Off** under **Other calendars**.
 ##### Add an event to the PTO calendar
 From the Google calendar associated with your @pyrofex.net account, create a new event for your time off. Invite **Personal Time Off** to add this event to the PTO calendar.
